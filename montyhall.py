#!/usr/bin/env python

import logging
from random import choice as choose
from random import seed as seed

logging.basicConfig(format="%(level)s:%(message)s")

# inputs
n_tests = 100000
seed()
doors = ['door 1', 'door 2', 'door 3']
strategies = ['rand', 'switch', 'noswitch']


# simulated game
def process():
    logging.info('new game')
    # car door
    car_door = choose(doors)
    logging.info('car door:', car_door)

    # first choice
    first_door = choose(doors)
    logging.info('first door:', first_door)

    # open a door
    remaining_doors = list(doors)
    remaining_doors.remove(first_door)
    if first_door != car_door:
        remaining_doors.remove(car_door)
    opened_door = remaining_doors[0]
    logging.info('opened door:', opened_door)

    # second choice random
    remaining_doors = list(doors)
    remaining_doors.remove(opened_door)
    second_door_rand = choose(remaining_doors)
    logging.info('second door rand:', second_door_rand)

    # second choice switch
    remaining_doors = list(doors)
    remaining_doors.remove(opened_door)
    remaining_doors.remove(first_door)
    second_door_switch = remaining_doors[0]
    logging.info('second door switch:', second_door_switch)

    # second choice noswitch
    second_door_noswitch = first_door
    logging.info('second door noswitch:', second_door_noswitch)

    return {strategies[0]: second_door_rand == car_door,
            strategies[1]: second_door_switch == car_door,
            strategies[2]: second_door_noswitch == car_door}


if __name__ == "__main__":
    # conduct an experiment
    verbose = False
    if verbose:
        logging.basicConfig(level=logging.DEBUG)

    win = {}

    for strat in strategies:
        logging.info(strat)
        win[strat] = 0

    for i in range(n_tests):
        result = process()
        for strat in strategies:
            if (result[strat]):
                win[strat] += 1

    for strat in strategies:
        print 'wins with strategy {strat}: {p:.0%}'.format(strat=strat,
                                            p=float(win[strat]) / n_tests)
